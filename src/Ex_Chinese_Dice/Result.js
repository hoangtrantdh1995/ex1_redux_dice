import React, { Component } from 'react'
import { connect } from 'react-redux'
import { resultDice } from '../redux/action/diceAction'

class Result extends Component {

    playGame = () => {
        console.log('Loading on');
        // tạo array mới 
        let newArrayDice = this.props.arrayDice.map(() => {
            // Math.floor chỉ trả từ 0 đến 0.9 nên phải cộng 1
            let random = Math.floor(Math.random() * 6) + 1;
            return {
                img: `./imgXucXac/${random}.png`,
                value: random,
            }
        })
        // sau 5s
        setTimeout(() => {
            console.log('Loading off ');
            this.props.showResult(newArrayDice)
        }, 2000)
    };

    render() {
        return (
            <div className='text-center pt-3 display-4'>
                <button
                    disabled={this.props.chooseDice == ''}
                    onClick={() => { this.playGame() }}
                    className='btn btn-secondary btn-lg'>
                    <span>Play Game</span>
                </button>
                <div className='pt-1'>
                    <h2 className='text-primary'>{this.props.resultPlayGame}</h2>
                </div>
                <div className='pt-1'>
                    {this.props.chooseDice ? (<h2 className='text-secondary'>Bạn chọn: {this.props.chooseDice}</h2>) : null}
                    <h3 className='text-danger'>Số lần bạn thắng : {this.props.totalWin}</h3>
                    <h3>Số lần bạn thua :{this.props.totalLose} </h3>
                    {this.props.totalPlayGame ? (<h3 className='text-primary'>Số lần play game : {this.props.totalPlayGame}</h3>) : null}

                    {/* <h3>Số lần play game : {this.props.totalPlayGame}</h3> */}
                    {/* {this.props.totalWin ? (<h3>Số lần bạn thắng : {this.props.totalWin}</h3>) : null}
                    {this.props.totalLose ? (<h3>Số lần bạn thua :{this.props.totalLose} </h3>) : null}
                    {this.props.totalPlayGame ? (<h3>Số lần play game : {this.props.totalPlayGame}</h3>) : null} */}
                </div>
            </div>
        )
    }
};

let mapStateToProps = (state) => ({
    arrayDice: state.diceReducer.arrayDice,
    resultPlayGame: state.diceReducer.resultPlayGame,
    totalWin: state.diceReducer.totalWin,
    totalLose: state.diceReducer.totalLose,
    totalPlayGame: state.diceReducer.totalPlayGame,
    chooseDice: state.diceReducer.chooseDice,

});

let mapDispatcchToProps = (dispatch) => ({
    showResult: (dice) => {
        dispatch(resultDice(dice))
    }
});

export default connect(mapStateToProps, mapDispatcchToProps)(Result);
