import React, { Component } from 'react'
import bg_game from "../assets/bgGame.png"
import Dice from './Dice'
import Result from './Result'
import "./gameDice.css"

export default class Ex_Chinese_Dice extends Component {
    render() {
        return (
            <div
                style={{
                    backgroundImage: `url(${bg_game})`,
                    width: "100vw",
                    height: "100vh",
                }}
                className="bg_game">
                <Dice />
                <Result />
            </div>
        )
    }
}
