import logo from './logo.svg';
import './App.css';
import Ex_Chinese_Dice from './Ex_Chinese_Dice/Ex_Chinese_Dice';

function App() {
  return (
    <div className="App">
      <Ex_Chinese_Dice />
    </div>
  );
}

export default App;
