import { CHOOSE_DICE, RESULT_DICE } from "../constant/diceConstant";


export const resultDice = (dice) => ({
    type: RESULT_DICE,
    payload: dice,
})

export const chooseDice = (dice) => ({
    type: CHOOSE_DICE,
    payload: dice,
})



