import { CHOOSE_DICE, RESULT_DICE } from "../constant/diceConstant";


let initialState = {
    arrayDice: [
        {
            img: "./imgXucXac/1.png",
            value: 1
        },
        {
            img: "./imgXucXac/2.png",
            value: 2
        },
        {
            img: "./imgXucXac/3.png",
            value: 3
        },
        // {
        //     img: "./imgXucXac/4.png",
        //     value: 4
        // },
        // {
        //     img: "./imgXucXac/5.png",
        //     value: 5
        // },
        // {
        //     img: "./imgXucXac/6.png",
        //     value: 5
        // },
    ],
    totalWin: 0,
    totalLose: 0,
    totalPlayGame: 0,
    chooseDice: "",
    resultPlayGame: "",
};

export const diceReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case CHOOSE_DICE: {
            return { ...state, chooseDice: payload }

        }
        case RESULT_DICE: {
            const point = payload.reduce((total, item) => {
                total += item.value;
                return total
            }, 0)

            let totalWin = state.totalWin
            let totalLose = state.totalLose
            let totalPlayGame = state.totalPlayGame
            let resultPlayGame = 'You LOSE'
            if (state.chooseDice == 'TAI' && point >= 11) {
                totalWin++
                resultPlayGame = 'You WIN'
            } else if (state.chooseDice == 'XIU' && point < 11) {
                totalWin++
                resultPlayGame = 'You WIN'
            } else {
                totalLose++
            }
            totalPlayGame = totalWin + totalLose;

            return { ...state, arrayDice: payload, totalPlayGame, resultPlayGame, totalWin, totalLose, totalPlayGame }
        }

        default:
            return state
    }
};


